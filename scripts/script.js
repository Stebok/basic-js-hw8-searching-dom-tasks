// DONE - Знайти всі параграфи на сторінці та встановити колір фону #ff0000
// OPTION 1
const elsP = document.querySelectorAll("p");
// переберём выбранные элементы
elsP.forEach((el) => {
  // установим каждому элементу background-color="#ff0000"
  el.style.backgroundColor = "#ff0000";
});

// // OPTION 2
// // получим все элементы p на странице
// const elsP = document.querySelectorAll('p');
// // for
// for (let i = 0, length = elsP.length; i < length; i++) {
//   elsP[i].style.backgroundColor = '#ff0000';
// }

// // OPTION 3
// // получим все элементы p на странице
// const elsP = document.querySelectorAll('p');
// //  for...of
// for (let el of elsP) {
//   el.style.backgroundColor = '#ff0000';
// }

// DONE Знайти елемент із id="optionsList". Вивести у консоль.
// Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let elemId = document.getElementById("optionsList");
console.log(elemId);

let elemParent = document.getElementById("optionsList").parentNode;
console.log(elemParent);

let elemChild = document.getElementById("optionsList");
// Сначала проверяем, что элемент имеет дочерние узлы
if (elemChild.hasChildNodes()) {
  let nodes = elemChild.children;
  // Проходим по списку узлов и печатаем их имена и тип
  for (let i = 0; i < nodes.length; i++) {
    console.log(
      "Дочерняя нода c названием " +
        nodes[i].nodeName +
        " и типом " +
        nodes[i].nodeType
    );
  }
}

// // DONE - Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// // This is a paragraph
const changeTextContent = document.querySelector("#testParagraph");
console.log(changeTextContent);
// эта строчка удалит все элементы в testParagraph и добавит в него контэнт с текстом "This is a paragraph"
changeTextContent.textContent = "This is a paragraph";
console.log(changeTextContent);

// // DONE Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// // Кожному з елементів присвоїти новий клас nav-item.
let parent = document.querySelector(".main-header");
let child = parent.children;
console.log(child);
for (let i = 0; i < child.length; i++) {
  child[i].classList.add("nav-item");
}

// // DONE - Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let elems = document.querySelectorAll(".section-title");
// console.log(elems)
[].forEach.call(elems, function (el) {
  el.classList.remove("section-title");
});
console.log(elems);
